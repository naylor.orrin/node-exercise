const request = require('request');
const numOfPeople = 87;
const numOfPlanets = 61;

const requestCache = {}

/*
* If you wanted to get the dada using the "next" property this is how you would do it
*/
const getAllFromStarwarsRecursive = async (type) => {
    if (type !== 'people' && type !== 'planets') {
        throw { message: `Only types of people and plates are allowed in getAllFromStarwars: ${type} is not allowed` }
    }
    const pageUrl = `https://swapi.co/api/${type}/`
    return await recursiveRequest(pageUrl)

}
const recursiveRequest = async (url) => {
    const fullResponcePromise = new Promise((resolve, reject) => {
        request(url, (err, response, body) => {
            if (err || !body) {
                reject(err)
                return
            }
            try {
                const starWarsResponse = JSON.parse(body)
                if (!starWarsResponse || !starWarsResponse.results || starWarsResponse.results.length === 0) {
                    throw ({
                        message: `Nothing was found when getting one of the groups of ${type} in getAllFromStarwars at endpoint ${url}`
                    })
                }
                resolve(starWarsResponse)
            }
            catch (e) {
                reject(e)
            }
        })
    })
    const fullResponce = await fullResponcePromise
    if (fullResponce.next) {
        const nextPageResponce = await recursiveRequest(fullResponce.next)
        return fullResponce.results.concat(nextPageResponce)
    }
    return fullResponce.results
}

const getAllFromStarwars = async (type) => {
    if (type !== 'people' && type !== 'planets') {
        throw { message: `Only types of people and plates are allowed in getAllFromStarwars: ${type} is not allowed` }
    }
    const numOfType = type === 'people' ? numOfPeople : numOfPlanets;
    let numberOfPages = Math.ceil(numOfType / 10);
    const arrayOfPageNumbers = [...Array(numberOfPages).keys()]
    // There is nothing in page zero so set page zero equat to the highest page
    arrayOfPageNumbers[0] = arrayOfPageNumbers.length

    const peopleRequestPromises = arrayOfPageNumbers.map(
        pageNum => {
            const pageUrl = `https://swapi.co/api/${type}/?page=${pageNum}`
            return new Promise((resolve, reject) => {
                request(pageUrl, (err, response, body) => {
                    if (err || !body) {
                        reject(err)
                        return
                    }
                    try {
                        const starWarsResponse = JSON.parse(body)
                        if (!starWarsResponse || !starWarsResponse.results || starWarsResponse.results.length === 0) {
                            throw ({
                                message: `Nothing was found when getting one of the groups of ${type} in getAllFromStarwars at endpoint ${pageUrl}`
                            })
                        }
                        resolve(starWarsResponse.results)
                    }
                    catch (e) {
                        reject(e)
                    }
                })
            })
        }
    )
    const starWarsRequestResult = await Promise.all(peopleRequestPromises)
    /* 
    * Right now each request has an array of results. We need to combine thoese into
    * one array
    */
    const starWars = starWarsRequestResult.reduce((allStarWars, starWarsResultsGroup) =>
        allStarWars.concat(starWarsResultsGroup), [])
    return starWars;
}

// I was just messing around and added some very basic caching options. It only chaches based on requst url, which has many problems.
const getAllFromStarwarsWithCache = async (type) => {
    if (type !== 'people' && type !== 'planets') {
        throw { message: `Only types of people and plates are allowed in getAllFromStarwars: ${type} is not allowed` }
    }
    const numOfType = type === 'people' ? numOfPeople : numOfPlanets;
    let numberOfPages = Math.ceil(numOfType / 10);
    const arrayOfPageNumbers = [...Array(numberOfPages).keys()]
    // There is nothing in page zero so set page zero equat to the highest page
    arrayOfPageNumbers[0] = arrayOfPageNumbers.length

    const peopleRequestPromises = arrayOfPageNumbers.map(
        pageNum => {
            const pageUrl = `https://swapi.co/api/${type}/?page=${pageNum}`
            return new Promise((resolve, reject) => {
                if (requestCache[pageUrl]) {
                    resolve(requestCache[pageUrl].results)
                    return;
                }
                request(pageUrl, (err, response, body) => {
                    if (err || !body) {
                        reject(err)
                        return
                    }
                    try {
                        const starWarsResponse = JSON.parse(body)
                        if (!starWarsResponse || !starWarsResponse.results || starWarsResponse.results.length === 0) {
                            throw ({
                                message: `Nothing was found when getting one of the groups of ${type} in getAllFromStarwars at endpoint ${pageUrl}`
                            })
                        }
                        requestCache[pageUrl] = starWarsResponse;
                        resolve(starWarsResponse.results)
                    }
                    catch (e) {
                        reject(e)
                    }
                })
            })
        }
    )
    const starWarsRequestResult = await Promise.all(peopleRequestPromises)
    /* 
    * Right now each request has an array of results. We need to combine thoese into
    * one array
    */
    const starWars = starWarsRequestResult.reduce((allStarWars, starWarsResultsGroup) =>
        allStarWars.concat(starWarsResultsGroup), [])
    return starWars;
}
const buildCache = async () => {
    await getAllFromStarwarsWithCache('people')
    await getAllFromStarwarsWithCache('planets')
    console.log("cache built")
}
buildCache()
module.exports = { getAllFromStarwarsRecursive, getAllFromStarwars, getAllFromStarwarsWithCache }