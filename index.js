const express = require('express');
const cors = require('cors');
const {
    getAllFromStarwarsRecursive,
    getAllFromStarwars,
    getAllFromStarwarsWithCache
} = require('./starWarsAPILayer')
const app = express();
/*
*  I am makin a huge assumtion in this code that there are only ever 87 
*  people and 61 planets. 
*  A significantly slower, but probbably a more stable solution would be to use the 
*  "next" link provided in the request
*  Which I did implement. You just need to replace 
*  getAllFromStarwars with getAllFromStarwarsRecursive
*  
*  I was messing around this afternoon and added a cache just replace
*  getAllFromStarwars with getAllFromStarwarsWithCache
*/
app.use(cors());

app.get('/people/', async (req, res) => {
    const startTime = Date.now()
    try {
        const { sortBy } = req.query
        const allPeople = await getAllPeople()
        if (!sortBy) {
            res.json(allPeople)
        }
        else if (sortBy === 'name') {
            const peopleSortedByName = allPeople.sort(
                (person1, person2) => person1.name.localeCompare(person2.name)
            )
            res.json(peopleSortedByName)
        }
        else if (sortBy === 'weight' || sortBy === 'height') {
            const sortString = sortBy === 'weight' ? 'mass' : 'height'
            const peopleSortedByWeight = allPeople.sort(
                (person1, person2) => person1[sortString] - person2[sortString]
            )
            res.json(peopleSortedByWeight)
        }
        else {
            throw {
                message: `Invalid sort type ${sortBy}. In /getPeople/`
            }
        }
    }
    catch (err) {
        // PS I would never return the err like this in production code. It might contain 
        // sensitive info
        res.json(err)
        console.log(err)
    }
    const requestTime = Date.now() - startTime
    console.log(`Total request time for /people/ ${requestTime}ms`)
});

app.get('/planets/', async (req, res) => {
    const startTime = Date.now()
    try {
        const allPlanets = await getAllPlanets()
        const allPeople = await getAllPeople()

        const urlToPersonNameMap = {}
        allPeople.forEach(person => {
            urlToPersonNameMap[person.url] = person.name
        })
        const planetsWithResendentsNames = allPlanets.map(planet => {
            const resendentsNames = planet.residents.map(resident => urlToPersonNameMap[resident])
            return { ...planet, residents: resendentsNames }
        })
        res.json(planetsWithResendentsNames)

    }
    catch (err) {
        // PS I would never return the err like this in production code. It might contain 
        // sensitive info
        res.json(err)
        console.log(err)
    }
    const requestTime = Date.now() - startTime
    console.log(`Total request time for /planets/ ${requestTime}ms`)
});
const getAllPeople = async () => {
    const allPeopleRequest = await getAllFromStarwars('people')
    return allPeopleRequest;
}
const getAllPlanets = async () => {
    const allPlanetsRequest = await getAllFromStarwars('planets')
    return allPlanetsRequest;
}

const server = app.listen(process.env.PORT || 3000, () => {
    const host = server.address().address,
        port = server.address().port;

    console.log('API listening at http://%s:%s', host, port);
});
